provider "aws" {
  region = "us-east-1"
}

terraform {
  required_version = ">=0.12.0"
  backend "s3" {
    encrypt        = true
    bucket         = "aws-amfam-sbx-aimlresearch-terraform-state-us-east-1"
    key            = "project_provisioning/athena-bq-conn.dev.tfstate"
    region         = "us-east-1"
    dynamodb_table = "aws-amfam-sbx-aimlresearch-terraform-lock-us-east-1"
  }
}


module "core-infra" {
  source      = "../../modules/core-infra"
  environment = "dev"
  local       = true
  defined_tags = {
    ApplicationID             = "athena-bq-conn"
    CreatedBy                 = "neha.godwal@amfam.com"
    CostCenter                = "12321"
    InformationClassification = "Confidential"
    InformationType           = "InP"
    InfrastructureTier        = "Development"
    Project                   = "athena-conn"
    SupportGroup              = "DTO Cloud Support"
  }
}
