# Secuirty Group
resource "aws_security_group" "this" {
  name_prefix = "${data.aws_iam_account_alias.current.account_alias}-athena-conn-${var.environment}-sg"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    Name = "athena-conn-${var.environment}"
  }
}

resource "aws_security_group_rule" "allow_22_in" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["10.0.0.0/8"]
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "allow_80_in" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["10.0.0.0/8"]
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "allow_443_in" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["10.0.0.0/8"]
  security_group_id = aws_security_group.this.id
}
# Egress: ALL
resource "aws_security_group_rule" "allow_all_out" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.this.id
}
