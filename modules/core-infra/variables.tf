
variable "defined_tags" {
  description = "A map of tags to add to all resources."
  type        = map(any)
  default     = {}
}

variable "environment" {
  type        = string
  default     = "dev"
  description = "API Gateway Stage"
}

variable "iam_additional_policy" {
  type        = string
  default     = ""
  description = "Additional IAM Policy for Lambda function"
}

variable "local" {
  type    = bool
  default = false
}
