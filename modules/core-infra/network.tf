################
# Networking
################
# VPC Selection
data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["${data.aws_iam_account_alias.current.account_alias}-vpc1*"]
  }
}

data "aws_subnet" "private_a" {
  filter {
    name   = "tag:Name"
    values = ["${data.aws_iam_account_alias.current.account_alias}-vpc1-private-a-subnet-${data.aws_region.current.name}a"]
  }
}
data "aws_subnet" "private_b" {
  filter {
    name   = "tag:Name"
    values = ["${data.aws_iam_account_alias.current.account_alias}-vpc1-private-a-subnet-${data.aws_region.current.name}b"]
  }
}

data "aws_subnet" "private_c" {
  filter {
    name   = "tag:Name"
    values = ["${data.aws_iam_account_alias.current.account_alias}-vpc1-private-a-subnet-${data.aws_region.current.name}c"]
  }
}
