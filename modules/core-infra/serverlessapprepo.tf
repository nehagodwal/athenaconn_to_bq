################################
# serverless application repo  
################################
# creating app - AthenaGoogleBigQueryConnector
resource "aws_serverlessapplicationrepository_cloudformation_stack" "athena-gbq-connector" {
  name           = "athena-gbq-connector"
  application_id = "arn:aws:serverlessrepo:us-east-1:292517598671:applications/AthenaGoogleBigQueryConnector"
  capabilities = [
    "CAPABILITY_IAM",
    "CAPABILITY_RESOURCE_POLICY",
  ]
  parameters = {
    SpillBucket = "igq-data-poc-bucket"
    GCPProjectID = "gcp-amfam-ds-research-dev"
    LambdaFunctionName = "bigquery-lambda"
    SecretNamePrefix = "arn:aws:secretsmanager:us-east-1:307326512814:secret:dev/poc/bigquery-ePQhhz"
    SecurityGroupIds = aws_security_group.this.vpc_id
    SubnetIds = "${data.aws_subnet.private_a.id}"
  }
}